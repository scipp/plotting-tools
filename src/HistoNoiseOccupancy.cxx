#include "HistoNoiseOccupancy.h"

//////////////
/// Public ///
//////////////
//==============================
HistoNoiseOccupancy::HistoNoiseOccupancy() :
Histo1D()
{
#ifdef DEBUG
    std::cout << "HistoNoiseOccupancy::HistoNoiseOccupancy()" << std::endl;
#endif
};
HistoNoiseOccupancy::~HistoNoiseOccupancy(){
#ifdef DEBUG
    std::cout << "HistoNoiseOccupancy::~HistoNoiseOccupancy()" << std::endl;
#endif
};
//==============================
///////////////
/// Private ///
///////////////
//==============================
void HistoNoiseOccupancy::setParametersX(TH2* i_h) {
#ifdef DEBUG
    std::cout << "HistoNoiseOccupancy::setParametersX(TH2)" << std::endl;
#endif
    m_axis.histo.x.nbins = 9;
    m_axis.histo.x.low   = 0;
    m_axis.histo.x.high  = 9;
    m_axis.histo.x.title = "Noise Occupancy";
    std::string label[9] = { "<1e-7", "1e-7<", "1e-6<", "1e-5<", "1e-4<", "1e-3<", "1e-2<", "1e-1<", "1<"};
    for (int i=0; i<9; i++) {
        m_axis.histo.x.label.push_back(label[i]);
    }
    m_thr = 1e-6;
}
void HistoNoiseOccupancy::drawHisto() {
#ifdef DEBUG
    std::cout << "HistoNoiseOccupancy::drawHisto()" << std::endl;
#endif
    Histo1D::drawHisto();

    m_c->cd();
    m_h->Draw("TEXT0 SAME");
    m_h->GetYaxis()->SetRangeUser(0,((m_h->GetBinContent(m_h->GetMaximumBin()))*1.25));
    m_c->Modified();
    m_c->Update();
};
void HistoNoiseOccupancy::fillHisto(json& i_j) {
#ifdef DEBUG
    std::cout << "HistoNoiseOccupancy::fillHisto(json)" << std::endl;
#endif
    int nbinsx = i_j["x"]["Bins"];
    int nbinsy = i_j["y"]["Bins"];
    m_zeros = 0;
    int fe = PlotTool::getFE();
    for (int row=0; row<nbinsy; row++) {
        for (int col=0; col<nbinsx; col++) {
            if (fe!=0&&PlotTool::whichFE(row, col)!=fe) continue;
            double tmp = i_j["Data"][col][row];
            int bin_num = whichBin(tmp);
            m_h->AddBinContent(bin_num);
        }
    }
}
void HistoNoiseOccupancy::fillHisto(TH2* i_h) {
#ifdef DEBUG
    std::cout << "HistoNoiseOccupancy::fillHisto(TH2)" << std::endl;
#endif
    int nbinsx = i_h->GetNbinsX();
    int nbinsy = i_h->GetNbinsY();
    m_zeros = 0;
    int fe = PlotTool::getFE();
    for (int row=0; row<nbinsy; row++) {
        for (int col=0; col<nbinsx; col++) {
            if (fe!=0&&PlotTool::whichFE(row, col)!=fe) continue;
            double tmp = i_h->GetBinContent(col+1, row+1);
            int bin_num = whichBin(tmp);
            m_h->AddBinContent(bin_num);
        }
    }
};
int HistoNoiseOccupancy::whichBin(double value) {
#ifdef DEBUG
    std::cout << "HistoNoiseOccupancy::whichBin(" << value << ")" << std::endl;
#endif
    int hist_bin = 1;
    const int num = 8;
    double par[num] = { 1e-7, 1e-6, 1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 1 };
    for (int i=0; i<num; i++) {
        if (value > par[i]) hist_bin++;
    }
    return hist_bin;
}
