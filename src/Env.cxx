#include "Env.h"

///////////////////////////////////////////////////////////////////////////

Env::Env(std::string i_name, TEnv* i_e) {
#ifdef DEBUG
    std::cout << "Env::Env(" << i_name << ", TEnv)" << std::endl;
#endif
    this->initialize(i_name);

    m_e = i_e;
    m_format = 0;
}
Env::Env(std::string i_name, json& i_j) {
#ifdef DEBUG
    std::cout << "Env::Env(" << i_name << ", json)" << std::endl;
#endif
    this->initialize(i_name);

    m_j = i_j;
    m_format = 1;
}
Env::Env(std::string i_name, std::string i_f) {
#ifdef DEBUG
    std::cout << "Env::Env(" << i_name << ", " << i_f << ")" << std::endl;
#endif
    this->initialize(i_name);

    m_f = i_f;
    m_format = 2;
}
Env::~Env() {
#ifdef DEBUG
    std::cout << "Env::~Env()" << std::endl;
#endif
    delete m_e;
}

void Env::initialize(std::string i_name) {
#ifdef DEBUG
    std::cout << "Env::initialize(" << i_name << ")" << std::endl;
#endif
    m_name = i_name;
    m_e = nullptr;
    m_j = nullptr;
    m_f = "";
    m_format = -1;
    m_run_e = false;
    m_run_j = false;
}

TEnv* Env::getEnv(std::string i_name) {
#ifdef DEBUG
    std::cout << "Env::getEnv(" << i_name << ")" << std::endl;
#endif
    if (m_run_e) return m_e;

    if (i_name=="") i_name = m_name;
    TEnv* e = new TEnv(i_name.c_str());

    if (m_format==0) {
        e = m_e;
    }
    else if (m_format==1) {
        e = this->jsonToEnv(e, m_j);
    }
    else if (m_format==2) {
        json j = fileToJson(m_f, m_name);
        if (j["is_null"]) j = nullptr;
        e = this->jsonToEnv(e, j);
    }

    m_e = e;
    m_run_e = true;

    return m_e;
}
json Env::getJson() {
#ifdef DEBUG
    std::cout << "Env::getJson()" << std::endl;
#endif
    if (m_run_j) return m_j;

    json j;

    if (m_format==0) {
        j = this->envToJson(m_e);
    }
    else if (m_format==1) {
        j = m_j;
    }
    else if (m_format==2) {
        j = fileToJson(m_f, m_name);
        if (j["is_null"]) j = nullptr;
    }

    m_j = j;
    m_run_j = true;

    return m_j;
}

void Env::setData(TEnv* i_e) {
#ifdef DEBUG
    std::cout << "Env::setData(TEnv)" << std::endl;
#endif
    this->initialize(m_name);
    m_e = i_e;
    m_format = 0;
}
void Env::setData(json& i_j) {
#ifdef DEBUG
    std::cout << "Env::setData(json)" << std::endl;
#endif
    this->initialize(m_name);
    m_j = i_j;
    m_format = 1;
}
void Env::setData(std::string i_f) {
#ifdef DEBUG
    std::cout << "Env::setData(" << i_f << ")" << std::endl;
#endif
    this->initialize(m_name);
    m_f = i_f;
    m_format = 2;
}

void Env::write(std::string i_name) {
#ifdef DEBUG
    std::cout << "Env::write(" << i_name << ")" << std::endl;
#endif
    if (i_name=="") i_name = m_name;

    TEnv* e = this->getEnv(i_name);
    e->Write(i_name.c_str());
}
void Env::writeFile(std::string i_name) {
#ifdef DEBUG
    std::cout << "Env::writeFile(" << i_name << ")" << std::endl;
#endif
    if (i_name=="") i_name = m_name;

    json j = this->getJson();
    std::ofstream o_file(i_name.c_str());
    o_file << std::setw(4) << j;
    o_file.close();
}

TEnv* Env::jsonToEnv(TEnv* i_e, json& i_j, std::string i_key) {
#ifdef DEBUG
    std::cout << "Env::jsonToEnv(TEnv, json, " << i_key << ")" << std::endl;
#endif
    if (i_j.is_null()) {
        i_e->SetValue( i_key.c_str(), "null" );
    }
    else if (i_j.is_number()) {
        i_e->SetValue( i_key.c_str(), std::to_string(i_j.get<float>()).c_str() );
    }
    else if (i_j.is_string()) {
        i_e->SetValue( i_key.c_str(), i_j.get<std::string>().c_str() );
    }
    else if (i_j.is_object()) {
        if (i_key!="") i_key += ".";
        for (auto& i: i_j.items()) {
            jsonToEnv( i_e, i.value(), i_key+i.key() );
        }
    }
    else if (i_j.is_array()) {
        if (i_key!="") i_key += ".";
        int cnt=0;
        for (auto& i: i_j.items()) {
            jsonToEnv( i_e, i.value(), i_key+std::to_string(cnt) );
            cnt++;
        }
    }
    return i_e;
}

json Env::envToJson(TEnv* i_e) {
#ifdef DEBUG
    std::cout << "Env::envToJson(TEnv)" << std::endl;
#endif
    json j;
    THashList* th = i_e->GetTable();
    TIter ti(th);
    TEnvRec* erec;
    while ((erec = (TEnvRec*) ti())) {
        std::string key = erec->GetName();
        std::string value = erec->GetValue();
        j[key] = value;
    }
    int max_size = 0;
    for (auto& i : j.items()) {
        std::string key = i.key();
        int key_size = split(key, '.').size();
        if (max_size<key_size) max_size = key_size;
    }
    for (int l=0; l<max_size; l++) {
        json out_j;
        int key_size = max_size-l;
        for (auto& i : j.items()) {
            std::string key = i.key();
            json value = i.value();
            if ((int)split(key, '.').size()<key_size||key_size==1) {
                out_j[key] = value;
            } else {
                std::size_t last = key.find_last_of('.');
                std::string tmp(key, 0, last);
                std::string substr(key, last+1, key.size());
                if (std::all_of(substr.cbegin(), substr.cend(), isdigit)) {
                    if (out_j[tmp].is_array()||out_j[tmp].is_null()) {
                        out_j[tmp].push_back(value);
                    } else {
                        out_j[tmp][substr] = value;
                    }
                } else {
                    if (out_j[tmp].is_array()) {
                        int cnt = 0;
                        json array_j;
                        for (auto in_array : out_j[tmp]) {
                            array_j[std::to_string(cnt)] = in_array;
                            cnt++;
                        }
                        out_j[tmp] = array_j;
                        out_j[tmp][substr] = value;
                    } else {
                        out_j[tmp][substr] = value;
                    }
                }
            }
        }
        j = out_j;
    }
    return j;
}

std::vector<std::string> split(std::string str, char del) {
    std::size_t first = 0;
    std::size_t last = str.find_first_of(del);

    std::vector<std::string> result;

    if (last==std::string::npos) {
        result.push_back(str);
    } else {
        while (first < str.size()) {
            std::string subStr(str, first, last - first);

            result.push_back(subStr);

            first = last + 1;
            last = str.find_first_of(del, first);

            if (last == std::string::npos) {
                last = str.size();
            }
        }
    }

    return result;
}

