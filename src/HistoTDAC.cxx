#include "HistoTDAC.h"
#include "Histo1D.h"

//////////////
/// Public ///
//////////////
//==============================
HistoTDAC::HistoTDAC() :
Histo2D(),
m_pS ( nullptr )
{
#ifdef DEBUG
    std::cout << "HistoTDAC::HistoTDAC()" << std::endl;
#endif
};
HistoTDAC::~HistoTDAC() {
#ifdef DEBUG
    std::cout << "HistoTDAC::~HistoTDAC()" << std::endl;
#endif
};
//==============================
///////////////
/// Private ///
///////////////
//==============================
void HistoTDAC::setParameters(TH2* i_h) {
#ifdef DEBUG
    std::cout << "HistoTDAC::setParameters(TH2)" << std::endl;
#endif
    m_histo_type = "Histo2d";

    this->setProjectionX(i_h);

    m_axis.histo.y.nbins = 31;
    m_axis.histo.y.low   = -15.5;
    m_axis.histo.y.high  = 15.5;
    m_axis.histo.y.title = "TDAC";
    m_axis.histo.z.title = "Number of Pixels";
}
void HistoTDAC::fillHisto(json& i_j) {
#ifdef DEBUG
    std::cout << "HistoTDAC::fillHisto(i_j)" << std::endl;
#endif
    int nbinsx = m_axis.data.x.nbins;
    int nbinsy = m_axis.data.y.nbins;
    int dist_xbin = m_axis.histo.x.nbins;
    double dist_xlow = m_axis.histo.x.low;
    double dist_xhigh = m_axis.histo.x.high;
    int fe = PlotTool::getFE();
    TH1* h[31];
    for (int i=0; i<31; i++) {
        h[i] = new TH1F(Form("htdac_%i",i),"",dist_xbin, dist_xlow, dist_xhigh);
        h[i]->GetXaxis()->SetTitle(m_axis.histo.x.title.c_str());
        h[i]->GetYaxis()->SetTitle(m_axis.histo.z.title.c_str());
    }
    for (int row=0; row<nbinsy; row++) {
        for (int col=0; col<nbinsx; col++) {
            if (fe!=0&&PlotTool::whichFE(row, col)!=fe) continue;
            double tmp = i_j["Data"][col][row];
            int tdac = m_map->GetBinContent(col+1, row+1);
            m_h->Fill(tmp, tdac);
            h[tdac+15]->Fill(tmp);
        }
    }
    m_pS.reset( new HistoTDACStack() );
    m_pS->setChip(m_chip_type, m_chip_name, m_fe_name);
    for (int i=0; i<31; i++) {
        m_pS->setHisto(h[i], std::to_string(i-15));
        delete h[i];
    }
};
void HistoTDAC::fillHisto(TH2* i_h) {
#ifdef DEBUG
    std::cout << "HistoTDAC::fillHisto(TH2)" << std::endl;
#endif
    int nbinsx = i_h->GetNbinsX();
    int nbinsy = i_h->GetNbinsY();
    int dist_xbin = m_axis.histo.x.nbins;
    double dist_xlow = m_axis.histo.x.low;
    double dist_xhigh = m_axis.histo.x.high;
    int fe = PlotTool::getFE();
    TH1* h[31];
    for (int i=0; i<31; i++) {
        h[i] = new TH1F(Form("htdac_%i",i),"",dist_xbin, dist_xlow, dist_xhigh);
        h[i]->GetXaxis()->SetTitle(m_axis.histo.x.title.c_str());
        h[i]->GetYaxis()->SetTitle(m_axis.histo.z.title.c_str());
    }
    for (int row=0; row<nbinsy; row++) {
        for (int col=0; col<nbinsx; col++) {
            if (fe!=0&&PlotTool::whichFE(row, col)!=fe) continue;
            double tmp = i_h->GetBinContent(col+1, row+1);
            int tdac = m_map->GetBinContent(col+1, row+1);
            m_h->Fill(tmp, tdac);
            h[tdac+15]->Fill(tmp);
        }
    }
    m_pS.reset( new HistoTDACStack() );
    m_pS->setChip(m_chip_type, m_chip_name, m_fe_name);
    for (int i=0; i<31; i++) {
        m_pS->setHisto(h[i], std::to_string(i-15));
        delete h[i];
    }
};
void HistoTDAC::drawHisto() {
#ifdef DEBUG
    std::cout << "HistoTDAC::drawHisto()" << std::endl;
#endif
    m_c->cd();
    style_TH2((TH2*)m_h);
    double min = m_h->GetMinimum(1);
    double max = m_h->GetMaximum();
    if (max<=min) max=min+1;
    m_h->GetZaxis()->SetRangeUser(min, max);
    if (m_axis.overwrite) {
        m_h->GetXaxis()->SetRangeUser(m_axis.histo.x.low, m_axis.histo.x.high);
        m_h->GetYaxis()->SetRangeUser(m_axis.histo.y.low, m_axis.histo.y.high);
        m_h->GetZaxis()->SetRangeUser(m_axis.histo.z.low, m_axis.histo.z.high);
    }
    m_h->Draw("colz");
    m_c->Modified();
    m_c->Update();
};
void HistoTDAC::printHisto(std::string i_dir, bool i_print, std::string i_ext) {
#ifdef DEBUG
    std::cout << "HistoTDAC::printHisto(" << i_dir << ", " << i_print << ", " << i_ext << ")" << std::endl;
#endif
    Histo2D::printHisto(i_dir, i_print, i_ext);

    if (m_pS) {
        m_pS->build();
        m_pS->write();
        std::string t = m_title + "_Stack";
        m_pS->setTitle(t);
        m_pS->print(i_dir, i_print, i_ext);
    }
}
