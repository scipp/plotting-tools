#include <glob.h>
#include <time.h>

#include "RootFile.h"
#include "Env.h"
#include "ReadFile.h"
#include "PlotTool.h"
#include "Histo1D.h"
#include "Histo2D.h"
#include "HistoProjection.h"
#include "HistoGaus.h"
#include "HistoRms.h"
#include "HistoOccupancy.h"
#include "HistoNoiseOccupancy.h"
#include "HistoStack.h"
#include "HistoTDAC.h"


//////////////
/// Public ///
//////////////
RootFile::RootFile():
m_file(NULL),
m_infile(NULL),
m_output(false),
m_input(false),
m_testtype(""),
m_chiptype(""),
m_chips(NULL),
m_print(false),
m_ext(""),
m_dir("")
{
#ifdef DEBUG
    std::cout << "RootFile::RootFile()" << std::endl;
#endif
}
RootFile::~RootFile() {
#ifdef DEBUG
    std::cout << "RootFile::~RootFile()" << std::endl;
#endif
    if (m_input) {
        std::string path_clone = m_infile->GetName();
        m_infile->Close();
        delete m_infile;
        std::string cmd = "rm " + path_clone;
        if (system(cmd.c_str())<0) {
            std::cerr << "[WARNING] Could not remove " << path_clone << std::endl;
        }
        for (auto iterator_f : *m_file->GetListOfKeys()) {
            std::string chip_name = iterator_f->GetName();
            if (!m_file->Get(chip_name.c_str())->IsFolder()) continue;
            TCanvas* c = new TCanvas("c","", 800, 600);
            std::string path = m_dir + "/" + chip_name + ".pdf";
            c->Print((path+"]").c_str(), "pdf");
            delete c;
        }
    }
    if (m_output) {
        m_file->Close();
        delete m_file;
    }
}
//==============================
void RootFile::recreate(std::string i_path) {
#ifdef DEBUG
    std::cout << "RootFile::recreate(" << i_path << ")" << std::endl;
#endif
    m_file = new TFile(i_path.c_str(), "RECREATE");
    m_output = true;
}
void RootFile::recreate(std::string i_path, std::string i_dir) {
#ifdef DEBUG
    std::cout << "RootFile::recreate(" << i_path << ", " << i_dir << ")" << std::endl;
#endif
    this->recreate(i_path);
    m_dir = i_dir;
    for (auto iterator_f : *m_infile->GetListOfKeys()) {
        std::string chip_name = iterator_f->GetName();
        if (!m_infile->Get(chip_name.c_str())->IsFolder()) continue;
        TCanvas* c = new TCanvas("c","", 800, 600);
        std::string path = m_dir + "/" + chip_name + ".pdf";
        c->Print((path+"[").c_str(), "pdf");
        delete c;
    }
}
int RootFile::load(std::string i_path) {
#ifdef DEBUG
    std::cout << "RootFile::load(" << i_path << ")" << std::endl;
#endif
    if (!checkFile(i_path, "error")) return -1;

    time_t t = time(NULL);
    std::string path_clone = i_path + std::to_string((int)t) + ".root";
    std::string cmd = "cp " + i_path + " " + path_clone;
    if (system(cmd.c_str())<0) {
        std::cerr << "[ERROR] Could not clone " << i_path << " to " << path_clone << std::endl;
        return -1;
    }
    m_infile = new TFile(path_clone.c_str());
    m_infile->SetName(path_clone.c_str());
    m_input = true;

    return 0;
}
void RootFile::setPrint(std::string i_ext) {
#ifdef DEBUG
    std::cout << "RootFile::setPrint(" << i_ext << ")" << std::endl;
#endif
    m_print = true;
    m_ext = i_ext;
}
//==============================
int RootFile::writeScanLog(std::string i_dir) {
#ifdef DEBUG
    std::cout << "RootFile::writeScanLog(" << i_dir << ")" << std::endl;
#endif
    std::string path = i_dir + "/scanLog.json";
    if (!checkFile(path, "error")) return -1;

    std::unique_ptr<Env> e ( new Env("scanLog", path) );
    json j = e->getJson();
    if (!j["testType"].is_null()) {
        m_testtype = j["testType"];
    } else {
        std::cerr << "[ERROR] Could not read 'testType' from " << path << std::endl;
        return -1;
    }
    if (!j["chipType"].is_null()) {
        m_chiptype = j["chipType"];
    } else {
        std::cerr << "[ERROR] Could not read 'chipType' from " << path << std::endl;
        return -1;
    }
    if (!j["chips"].is_null()) {
        m_chips = j["chips"];
    } else {
        std::cerr << "[ERROR] Could not read 'chips' from " << path << std::endl;
        return -1;
    }
    m_file->cd();
    e->write();
    return 0;
}
int RootFile::writeScanCfg(std::string i_dir) {
#ifdef DEBUG
    std::cout << "RootFile::writeScanCfg(" << i_dir << ")" << std::endl;
#endif
    std::string path = i_dir + "/" + m_testtype + ".json";
    if (checkFile(path, "warning")) {
        std::unique_ptr<Env> e ( new Env("scanCfg", path) );
        m_file->cd();
        e->write();
    }
    return 0;
}
int RootFile::writeChipData (std::string i_dir) {
#ifdef DEBUG
    std::cout << "RootFile::writeChipData(" << i_dir << ")" << std::endl;
#endif
    for (int num=0; num<(int)m_chips.size(); num++) {
        std::string chip_name = "DisabledChip_" + std::to_string(num);
        if (m_chips[num]["enable"].is_null()||m_chips[num]["enable"]!=0) {
            if (m_chips[num]["config"].is_null()) {
                std::cerr << "[ERROR] Could not read 'chips." << std::to_string(num) << ".config' from scanLog" << std::endl;
                return -1;
            }
            /// chip config
            std::string cfg_path = m_chips[num]["config"];
            std::string ext[2] = { "before", "after" };
            for (int file_num=0; file_num<2; file_num++) {
                std::string path = i_dir+"/"+cfg_path+"."+ext[file_num];
                std::string type = ext[file_num]+"Cfg";
                json chipCfg = readChipPixCfg(path, m_chiptype);
                if (!chipCfg["is_null"]) {
                    /// pixel config to TH2
                    chip_name = chipCfg["Name"];
                    /// mkdir for chip
                    TDirectory* d = this->mkdir((TDirectory*)m_file, chip_name, "Chip Name");
                    /// mkdir for before/after config
                    TDirectory* d2 = this->mkdir(d, type, type);
                    d2->cd();
                    for (auto& data : chipCfg["PixelConfig"].items()) {
                        json plotData = chipCfg["PixelConfig"][data.key()];
                        this->writePlotFromJson(chip_name, plotData, d2);
                    }
                    /// global config to TEnv
                    Env* e = new Env(type, path);
                    d2->cd();
                    e->write("Global");
                    delete e;
                }
            }
            /// chip data
            TDirectory* d3 = this->mkdir((TDirectory*)m_file, chip_name, "Chip Name");
            glob_t globbuf;
            glob((i_dir+"/"+chip_name+"*").c_str(), 0, NULL, &globbuf);
            for (int file_num=0; file_num<globbuf.gl_pathc; file_num++) {
                std::string path = globbuf.gl_pathv[file_num];
                json plotData = readDataFile(path, chip_name);
                this->writePlotFromJson(chip_name, plotData, d3);
            }
        } else {
            this->mkdir((TDirectory*)m_file, chip_name, "Chip Name");
        }
        m_chips[num]["name"] = chip_name;
    }
    return 0;
}
//==============================
int RootFile::loadScanLog() {
#ifdef DEBUG
    std::cout << "RootFile::loadScanLog()" << std::endl;
#endif
    if (!m_infile->Get("scanLog")) {
        std::cerr << "[ERROR] Could not find 'scanLog' Object" << std::endl;
        return -1;
    }
    Env* e = new Env("scanLog", (TEnv*)m_infile->Get("scanLog"));
    json j = e->getJson();
    if (!j["testType"].is_null()) {
        m_testtype = j["testType"];
    } else {
        std::cerr << "[ERROR] Could not read 'testType' from TEnv scanLog" << std::endl;
        return -1;
    }
    if (!j["chipType"].is_null()) {
        m_chiptype = j["chipType"];
    } else {
        std::cerr << "[ERROR] Could not read 'chipType' from TEnv scanLog" << std::endl;
        return -1;
    }
    m_file->cd();
    e->write();
    delete e;
    return 0;
}
int RootFile::loadScanCfg() {
#ifdef DEBUG
    std::cout << "RootFile::loadScanCfg()" << std::endl;
#endif
    if (!m_infile->Get("scanCfg")) {
        std::cerr << "[ERROR] Could not find 'scanCfg' Object" << std::endl;
        return -1;
    }
    Env* e = new Env("scanCfg", (TEnv*)m_infile->Get("scanCfg"));
    m_file->cd();
    e->write();
    delete e;
    return 0;
}
int RootFile::loadChipData() {
#ifdef DEBUG
    std::cout << "RootFile::loadChipData()" << std::endl;
#endif
    for (auto iterator_f : *m_infile->GetListOfKeys()) {
        std::string chip_name = iterator_f->GetName();
        if (!m_infile->Get(chip_name.c_str())->IsFolder()) continue;

        TDirectory* d = this->mkdir((TDirectory*)m_file, chip_name, "Chip Name");
        for (auto itr_d : *m_infile->GetDirectory(chip_name.c_str())->GetListOfKeys()) {
            std::string obj_name = itr_d->GetName();
            if (obj_name=="beforeCfg"||obj_name=="afterCfg") { /// chip config
                TDirectory* d2 = this->mkdir(d, obj_name, obj_name);
                for (auto itr_o : *m_infile->GetDirectory(chip_name.c_str())->GetDirectory(obj_name.c_str())->GetListOfKeys()) {
                    TObject* o =  m_infile->GetDirectory(chip_name.c_str())->GetDirectory(obj_name.c_str())->Get(itr_o->GetName());
                    std::string data_name = o->GetName();
                    std::string data_type = o->GetTitle();
                    if (data_name=="TEnv") { /// global config
                        Env* e = new Env(data_name, (TEnv*)o);
                        d2->cd();
                        e->write("Global");
                        delete e;
                    } else { /// pixel config
                        TDirectory* d_in = m_infile->GetDirectory(chip_name.c_str())->GetDirectory(obj_name.c_str())->GetDirectory(data_name.c_str());
                        TDirectory* d3 = this->mkdir(d2, data_name, data_type);
                        d3->cd();
                        this->writePlotFromDir(chip_name, d_in, d3);
                    }
                }
            } else { /// plots
                TObject* o = m_infile->GetDirectory(chip_name.c_str())->Get(obj_name.c_str());
                std::string data_name = o->GetName();
                std::string data_type = o->GetTitle();
                TDirectory* d_in = m_infile->GetDirectory(chip_name.c_str())->GetDirectory(data_name.c_str());

                TDirectory* d2 = this->mkdir(d, data_name, data_type);
                d2->cd();
                this->writePlotFromDir(chip_name, d_in, d2);
            }
        }
    }
    return 0;
}

///////////////
/// Private ///
///////////////
void RootFile::writePlotFromJson(std::string i_name, json& i_data, TDirectory* i_d, std::string i_fe) {
#ifdef DEBUG
    std::cout << "RootFile::writePlotFromJson(" << i_name << ", i_data, i_d, " << i_fe << ")" << std::endl;
#endif
    if ((!i_data["is_null"].is_null()&&i_data["is_null"])||i_data["Type"].is_null()||i_data["Name"].is_null()) return;

    std::unique_ptr<PlotTool> pH( nullptr );
    std::string data_type = i_data["Type"];
    std::string title = "";
    if (data_type=="Histo1d") {
        pH.reset( new Histo1D() ); title = "Dist";
    } else if (data_type=="Histo2d") {
        pH.reset( new Histo2D() ); title = "Map";
    } else {
        return;
    }

    TDirectory* d = this->mkdir(i_d, i_data["Name"], title);

    /// Dist/Map
    pH->setChip(m_chiptype, i_name, i_fe);
    pH->setData(i_data, title);
    pH->build();
    d->cd();
    pH->write();

    std::string axisX = i_data["x"]["AxisTitle"];
    std::string axisY = i_data["y"]["AxisTitle"];
    if ((axisX!="Column"&&axisX!="Col")||(axisY!="Row"&&axisY!="Rows")) return;

    if (i_fe==""&&m_chiptype=="RD53A") {
        std::vector<std::string> AFEs = { "Syn", "Lin", "Diff" };
        for (auto AFE : AFEs) {
            this->writePlotFromJson(i_name, i_data, i_d, AFE);
        }
    }
}
//==============================
void RootFile::writePlotFromDir(std::string i_name, TDirectory* i_d_in, TDirectory* i_d_out, std::string i_fe) {
#ifdef DEBUG
    std::cout << "RootFile::writePlotFromDir(" << i_name << ", i_d_in, i_d_out, " << i_fe << ")" << std::endl;
#endif
    std::string data_name = i_d_in->GetName();
    std::string data_type = i_d_in->GetTitle();
    std::string t;

    /// TH1/TH2
    std::unique_ptr<PlotTool> pH( nullptr );
    if (data_type=="Dist")     pH.reset( new Histo1D() );
    else if (data_type=="Map") pH.reset( new Histo2D() );
    else                       return;

    pH->setChip(m_chiptype, i_name, i_fe);

    TObject* map = i_d_in->Get(data_type.c_str());
    if (i_fe!="") data_type = data_type + "_" + i_fe;
    TObject* o = i_d_in->Get(data_type.c_str());
    if (data_type.find("Dist")!= std::string::npos)     pH->setHisto((TH1*)o, data_type);
    else if (data_type.find("Map")!= std::string::npos) pH->setHisto((TH2*)o, data_type);

    i_d_out->cd();
    pH->write();
    t = data_name + "_" + data_type;
    pH->setTitle(t);
    pH->print(m_dir, m_print, m_ext);

    std::string axisX = ((TH1*)map)->GetXaxis()->GetTitle();
    std::string axisY = ((TH1*)map)->GetYaxis()->GetTitle();
    if ((axisX!="Column"&&axisX!="Col")||(axisY!="Row"&&axisY!="Rows")) return;

    /// Projection
    std::unique_ptr<PlotTool> pP( new HistoProjection() );
    std::string pro_name = "Projection";
    if (i_fe!="") pro_name = pro_name + "_" + i_fe;
    pP->setChip(m_chiptype, i_name, i_fe);
    pP->setData((TH2*)map, map->GetName(), "Projection");
    pP->build();
    i_d_out->cd();
    pP->write();

    std::vector<std::string> types_1d;
    std::vector<std::string> types_2d;
    if (data_name.find("OccupancyMap") != std::string::npos )   types_1d.push_back("Occupancy");
    if (data_name.find("NoiseOccupancy") != std::string::npos ) types_1d.push_back("NoiseOccupancy");
    if ( data_name.find("MeanTotMap")   != std::string::npos ||
         data_name.find("SigmaTotMap")  != std::string::npos ||
         data_name.find("ThresholdMap") != std::string::npos ||
         data_name.find("NoiseMap")     != std::string::npos )  types_1d.push_back("Rms");
    if ( data_name.find("ThresholdMap") != std::string::npos ||
         data_name.find("NoiseMap")     != std::string::npos )  types_1d.push_back("Gaus");
    if ( data_name.find("ThresholdMap") != std::string::npos ) types_2d.push_back("TDAC");
    bool set_rms = false;
    bool set_gaus = false;
    for (auto type : types_1d) {
        this->writePlotFromObj(i_name, map, i_d_out, type, i_fe);
        if (type=="Rms"||type=="Gaus") set_rms = true;
        if (type=="Gaus") set_gaus = true;
    }
    for (auto type : types_2d) {
        this->writePlotFromObj(i_name, map, i_d_out, type, i_fe);
    }
    if (set_rms) pP->setRms();
    if (set_gaus) {
        pP->setMap((TH2*)map);
        std::string fit_name = "Fit_Gaus";
        if (i_fe!="") fit_name = fit_name + "_" + i_fe;
        pP->setGaus((TF1*)i_d_out->Get(fit_name.c_str()));
    }
    //pP->setZero();
    t = data_name + "_Projection";
    if (i_fe!="") t = t + "_" + i_fe;
    pP->setTitle(t);
    pP->print(m_dir, m_print, m_ext);

    if (i_fe==""&&m_chiptype=="RD53A") {
        std::vector<std::string> AFEs = { "Syn", "Lin", "Diff" };
        for (auto AFE : AFEs) {
            this->writePlotFromDir(i_name, i_d_in, i_d_out, AFE);
        }
        /// THStack
        std::unique_ptr<PlotTool> pS ( nullptr );
        types_1d.push_back("Projection");
        for (auto type : types_1d) {
            pS.reset( new HistoStack() );
            pS->setChip(m_chiptype, i_name);
            for (auto AFE : AFEs) {
                std::string h_name = type + "_" + AFE;
                TH1* h = (TH1*)i_d_out->Get(h_name.c_str());
       	        pS->setHisto(h, AFE);
                delete h;
            }
            pS->build();
            i_d_out->cd();
            pS->write();
            t = data_name + "_" + type + "_Stack";
            pS->setTitle(t);
            pS->print(m_dir, m_print, m_ext);
        }
    }
}
//==============================
void RootFile::writePlotFromObj(std::string i_name, TObject* i_o, TDirectory* i_d, std::string i_type, std::string i_fe) {
#ifdef DEBUG
    std::cout << "RootFile::writePlotFromObj(" << i_name << ", i_o, i_d, " << i_type << ", " << i_fe << ")" << std::endl;
#endif
    std::string t;

    std::unique_ptr<PlotTool> pH ( nullptr );
    if      (i_type=="Projection")     pH.reset( new HistoProjection() );
    else if (i_type=="Occupancy")      pH.reset( new HistoOccupancy() );
    else if (i_type=="NoiseOccupancy") pH.reset( new HistoNoiseOccupancy() );
    else if (i_type=="Rms")            pH.reset( new HistoRms() );
    else if (i_type=="Gaus")           pH.reset( new HistoGaus() );
    else if (i_type=="TDAC")           pH.reset( new HistoTDAC() );
    else return;

    pH->setChip(m_chiptype, i_name, i_fe);
    pH->setData((TH2*)i_o, i_o->GetName(), i_type);
    if (i_type=="Rms"||i_type=="Gaus") {
        std::string pro_name = "Projection";
        if (i_fe!="") pro_name = pro_name + "_" + i_fe;
        TH1* h = (TH1*)i_d->Get(pro_name.c_str());
        pH->setDist((TH1*)h->Clone());
        if (i_type=="Gaus") pH->fitHisto((TH1*)h->Clone());
    } else if (i_type=="TDAC") {
        /// Pull chip config directory
        TDirectory* after_d = m_infile->GetDirectory(i_name.c_str())->GetDirectory("afterCfg");
        if (after_d!=nullptr) {
            TDirectory* tdac_d = after_d->GetDirectory("TDAC");
            if (tdac_d!=nullptr) {
                std::string tdac_name = "Map";
                if (i_fe!="") tdac_name = tdac_name + "_" + i_fe;
                TH2* h = (TH2*)tdac_d->Get(tdac_name.c_str());
                pH->setMap((TH2*)h->Clone());
            }
        }
    }
    pH->build();
    i_d->cd();
    pH->write();
    t = std::string(i_d->GetName()) + "_" + i_type;
    if (i_fe!="") t = t + "_" + i_fe;
    pH->setTitle(t);
    pH->print(m_dir, m_print, m_ext);
}
//==============================
TDirectory* RootFile::mkdir(TDirectory* i_mother, std::string i_name, std::string i_title) {
#ifdef DEBUG
    std::cout << "RootFile::mkdir(i_mother, " << i_name << ", " << i_title << ")" << std::endl;
#endif
    TDirectory* d;
    std::replace(i_name.begin(), i_name.end(), '-', '_');
    if (!i_mother->Get(i_name.c_str())) d = i_mother->mkdir(i_name.c_str(), i_title.c_str());
    else d = (TDirectory*)i_mother->Get(i_name.c_str());
    return d;
}
