#include "Histo1D.h"

//////////////
/// Public ///
//////////////
//==============================
Histo1D::Histo1D() {
#ifdef DEBUG
    std::cout << "Histo1D::Histo1D()" << std::endl;
#endif
};
Histo1D::~Histo1D(){
#ifdef DEBUG
    std::cout << "Histo1D::~Histo1D()" << std::endl;
#endif
};
//==============================
/////////////////
/// Protected ///
/////////////////
//==============================
void Histo1D::setParameters(json& i_j) {
    PlotTool::setParameters(i_j);
}
void Histo1D::setParameters(TH1* i_h) {
    PlotTool::setParameters(i_h);
}
void Histo1D::setParameters(TH2* i_h) {
#ifdef DEBUG
    std::cout << "Histo1D::setParameters(TH2)" << std::endl;
#endif
    m_histo_type = "Histo1d";
    m_axis.histo.y.nbins = 1;
    m_axis.histo.y.low   = 0;
    m_axis.histo.y.high  = 1;
    m_axis.histo.y.title = "Number of Pixels";
    m_axis.histo.z.title = "";
    this->setParametersX(i_h);
}
void Histo1D::setParametersX(TH2* i_h) {
#ifdef DEBUG
    std::cout << "Histo1D::setParametersX(TH2)" << std::endl;
#endif
    this->setProjectionX(i_h);
}
//==============================
void Histo1D::setHisto(TH1* i_h, std::string i_n) {
    PlotTool::setHisto(i_h, i_n);
}
void Histo1D::setHisto(TH2* i_h, std::string i_n) {
    PlotTool::setHisto(i_h, i_n);
}
//==============================
void Histo1D::writeHisto() {
    PlotTool::writeHisto();
}
void Histo1D::buildHisto() {
#ifdef DEBUG
    std::cout << "Histo1D::buildHisto()" << std::endl;
#endif
    PlotTool::buildHisto();

    int nbinsx  = m_axis.histo.x.nbins;
    float xlow  = m_axis.histo.x.low;
    float xhigh = m_axis.histo.x.high;
    m_h = new TH1F(m_histo_name.c_str(), "", nbinsx, xlow, xhigh);

    std::string xaxistitle = m_axis.histo.x.title;
    std::string yaxistitle = m_axis.histo.y.title;
    style_TH1((TH1*)m_h, xaxistitle.c_str(), yaxistitle.c_str(), m_color.at(this->getFE()));

    if (m_axis.histo.x.label.size()!=0) {
        for (int i=0; i<(int)m_axis.histo.x.label.size(); i++) m_h->GetXaxis()->SetBinLabel(i+1, m_axis.histo.x.label[i].c_str());
        m_h->GetXaxis()->LabelsOption("h");
        m_h->SetMarkerSize(1.8);
        m_h->SetMarkerColor(1);
    }
};
void Histo1D::setCanvas() {
#ifdef DEBUG
    std::cout << "Histo1D::setCanvas()" << std::endl;
#endif
    m_c = new TCanvas(Form("canv_%s",m_h->GetName()), "", 800, 600);
    style_TH1Canvas(m_c);
};
void Histo1D::drawHisto() {
#ifdef DEBUG
    std::cout << "Histo1D::drawHisto()" << std::endl;
#endif
    m_c->cd();
    style_TH1((TH1*)m_h, m_h->GetXaxis()->GetTitle(), m_h->GetYaxis()->GetTitle(), m_color.at(this->getFE()));
    m_h->Draw();
    m_h->GetXaxis()->SetRangeUser(m_axis.histo.x.low, m_axis.histo.x.high);
    m_h->GetYaxis()->SetRangeUser(0,(m_h->GetBinContent(m_h->GetMaximumBin()))*1.25);
    if (m_axis.histo.x.nbins==2) {
        m_h->GetXaxis()->SetNdivisions(2,0,0);
    }
    if (m_gaus) {
        style_TF1((TF1*)m_f);
        m_f->Draw("SAME");
        m_h->GetYaxis()->SetRangeUser(0,(m_h->GetBinContent(m_h->GetMaximumBin()))*1.7);
    }
    if (m_axis.overwrite) {
        m_h->GetYaxis()->SetRangeUser(m_axis.histo.y.low, m_axis.histo.y.high);
    }
    m_c->Modified();
    m_c->Update();
};
void Histo1D::printHisto(std::string i_dir, bool i_print, std::string i_ext) {
    PlotTool::printHisto(i_dir, i_print, i_ext);
}
void Histo1D::fillHisto(json& i_j) {
#ifdef DEBUG
    std::cout << "Histo1D::fillHisto(i_j)" << std::endl;
#endif
    int nbinsx = i_j["x"]["Bins"];
    for (int i=0; i<nbinsx; i++) {
        double tmp = i_j["Data"][i];
        m_h->SetBinContent(i+1,tmp);
    }
};
void Histo1D::fillHisto(TH1* i_h) {
#ifdef DEBUG
    std::cout << "Histo1D::fillHisto(i_h)" << std::endl;
#endif
    int nbinsx = i_h->GetNbinsX();
    for (int i=0; i<nbinsx; i++) {
        double tmp = i_h->GetBinContent(i+1);
        double par = i_h->GetBinCenter(i+1);
        m_h->SetBinContent(m_h->FindBin(par), tmp);
    }
};
void Histo1D::fillHisto(TH2* i_h) {};
