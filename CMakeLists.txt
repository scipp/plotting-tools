cmake_minimum_required(VERSION 3.0)

project(PlottingTools)

set(EXECUTABLE_OUTPUT_PATH "${PROJECT_BINARY_DIR}/bin")
if(NOT TARGET_INSTALL_AREA )
set(TARGET_INSTALL_AREA ${PROJECT_SOURCE_DIR})
endif()
install(DIRECTORY ${PROJECT_BINARY_DIR}/bin DESTINATION ${TARGET_INSTALL_AREA}  FILES_MATCHING PATTERN "*" PERMISSIONS OWNER_EXECUTE OWNER_WRITE OWNER_READ)

if(CMAKE_BUILD_TYPE MATCHES Asan)
    add_compile_options(-g -fsanitize=address -fsanitize=undefined)
    add_link_options(-fsanitize=address -fsanitize=undefined)
    message("Enabling address sanitizer")
elseif(CMAKE_BUILD_TYPE MATCHES Debug)
    add_compile_options(-g)
else()
    add_compile_options(-O2)
endif()
include_directories(${PROJECT_SOURCE_DIR}/include)

# ROOT
list(APPEND CMAKE_PREFIX_PATH $ENV{ROOTSYS})
set(CMAKE_MESSAGE_LOG_LEVEL "ERROR")
find_package(ROOT COMPONENTS Hist Graf)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${ROOT_CXX_FLAGS}")

set(CMAKE_MESSAGE_LOG_LEVEL "STATUS")
include_directories(${ROOT_INCLUDE_DIRS})
add_subdirectory(src)

file(GLOB exec tools/*.cxx)
foreach(target ${exec})
    get_filename_component(execname ${target} NAME_WE)
    get_filename_component(srcfile ${target} NAME)
    add_executable(${execname} tools/${srcfile})
    target_link_libraries(${execname} ${ROOT_LIBRARIES} PlotTools)
    set_target_properties(${execname} PROPERTIES LINKER_LANGUAGE CXX)
endforeach()

