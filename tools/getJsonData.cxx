// #################################
// # Author: Arisa Kubota
// # Email: arisa.kubota at cern.ch
// # Project: Yarr
// # Description: Get JSON data from ROOT file
// ################################

#include <TKey.h>

#include "PlotStyle.h"
#include "PlotTool.h"
#include "Histo1D.h"
#include "Histo2D.h"
#include "RootFile.h"
#include "Env.h"
/////

void printHelp();

// main function
int main(int argc, char *argv[]) {

    SetPlotStyle();

    /// Information
    std::cout << "\nThis command converts TENV data in the input ROOT file into JSON data file.\n" << std::endl;

    std::string i_file = "";
    std::string o_file = "";
    std::string i_key  = "";
    std::size_t pos;

    // get argument
    int c;
    while ((c = getopt(argc, argv, "Hhi:o:k:")) != -1) {
        switch (c) {
            case 'H':
                printHelp();
                return 0;
                break;
            case 'h':
                printHelp();
                return 0;
                break;
            case 'i':
                i_file = std::string(optarg);
                break;
            case 'o':
                o_file = std::string(optarg);
                break;
            case 'k':
                i_key = std::string(optarg);
                break;
            case '?':
                if(optopt=='o'){
                    std::cerr <<
                        "\033[33m-> Option " <<
                        (char)optopt <<
                        " requires a parameter! (Proceeding with default)\033[m" <<
                    std::endl;
                }else if(optopt=='i'){
                    std::cerr <<
                        "\033[31m-> Option " <<
                        (char)optopt <<
                        " requires a parameter! Aborting...\033[m" <<
                    std::endl;
                    return -1;
                } else {
                    std::cerr << "\033[33m-> Unknown parameter: " << (char)optopt << "\033[m" << std::endl;
                }
                break;
            default:
                std::cerr << "\033[31m-> Error while parsing command line parameters!\033[m" << std::endl;
                return -1;
        }
    }

    ///////////////////
    /// Input ROOT file
    if (i_file=="") {
        std::cerr << "Error: No ROOT file path given!" << std::endl;
        std::cerr << "       Please specify ROOT file path under -i option." << std::endl;
        return -1;
    }
    if (i_file.size()<6||i_file.substr(i_file.size()-5, i_file.size())!=".root") {
        std::cerr << "Error: Not ROOT file path: " << i_file << std::endl;
        std::cerr << "       Please specify ROOT file path under -i option." << std::endl;
        return -1;
    }

    ///////////////////////////////
    /// Select key to get JSON file
    TFile* i_rootfile = new TFile(i_file.c_str());
    TList* keys = i_rootfile->GetListOfKeys();
    if (i_key=="") {
        std::cout << "TEnv List: \n" << std::endl;
        for (auto l: *keys) {
            TKey* k = (TKey*)l;
            std::string name = k->GetClassName();
            if (name!="TEnv") continue;
            std::cout << "\t" << k->GetName() << std::endl;
        }
        std::cout << "\nEnter the name of TEnv Object to get. \n> ";
        std::cin >> i_key;
        std::cout << std::endl;
    }

    TObject* key = i_rootfile->Get(i_key.c_str());
    if (key==nullptr) {
        std::cerr << "\033[31mError: Not found " << i_key <<  " in " << i_file << "\033[m" << std::endl;
        return -1;
    }

    if (std::string(key->ClassName())!="TEnv") {
        std::cerr << "\033[31mError: " << i_key <<  " is " << key->ClassName() << ", not TEnv" << "\033[m" << std::endl;
        return -1;
    }

    ////////////////////
    /// Output JSON file
    std::string o_dir;
    pos = i_file.find_last_of('/');
    if (pos==std::string::npos) o_dir = ".";
    else o_dir = i_file.substr(0, pos);

    if (o_file=="") {
        std::cout << "Warning: No output file name given. Proceeding with default: " << o_dir << "/" << i_key << "_root.json" << std::endl;
        o_file = o_dir + "/" + i_key + "_root.json";
    }

#ifdef DEBUG
    ////////////////
    /// Confirmation
    std::cout << std::endl;
    std::cout << "*********************************" << std::endl;
    std::cout << "Input ROOT file: " << i_file << std::endl;
    std::cout << "Key: " << i_key << std::endl;
    std::cout << "Output JSON file: " << o_file << std::endl;
    std::cout << "*********************************" << std::endl;
    std::cout << std::endl;
#endif

    std::string cmd = "mkdir -p " + o_dir;
    if (system(cmd.c_str())!=0) {
        std::cerr << "\033[31mError: Problem in creating " << o_dir << "\033[m" << std::endl;
        return -1;
    }

    ////////////////
    /// TEnv to json
    Env* e = new Env(i_key.c_str(), (TEnv*)i_rootfile->Get(i_key.c_str()));
    json j = e->getJson();
    delete e;
    std::ofstream ofile(o_file.c_str());
    ofile << std::setw(4) << j;
    ofile.close();

    std::cout << "Output " << i_key << " in " << o_file << std::endl;

    i_rootfile->Close();

    return 0;
}

void printHelp() {
    std::cout << "Help:" << std::endl;
    std::cout << " -h/-H: Shows this." << std::endl;
    std::cout << " -i <file> : Input ROOT file name." << std::endl;
    std::cout << " -o <file> : Output file name. (Default. path/to/input/dir/output.json)" << std::endl;
    std::cout << " -k <key>  : Config file key if specified." << std::endl;
}
