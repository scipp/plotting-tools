#ifndef __READFILE_H
#define __READFILE_H

#include <math.h> //for fabs()
#include <utility> //for pairs
#include <vector>
#include <algorithm> //for std::sort()
#include <iomanip>
#include <unistd.h>

#include <iostream>
#include <fstream>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <TEnv.h>
#include <TROOT.h>
#include <THashList.h>

#include <json.hpp>
using json=nlohmann::basic_json<std::map, std::vector, std::string, bool, std::int32_t, std::uint32_t, float>;

/***
Convert dat file into json data
* i_path: path to dat file
***/
json datToJson(std::string /*i_path*/);

/***
Check/Modify json data of scanLog
* i_j: json data of scanLog
***/
json readScanLog(json& /*i_j*/);

/***
Check/Modify json data of chip global config
* i_j: json data of chip global config
***/
json readChipGlobCfg(json& /*i_j*/);

/***
Convert file into json data
* i_path: path to file
* i_type: type of file
    - scanLog
    - scanCfg
    - plotCfg
    - beforeCfg/afterCfg
***/
json fileToJson(std::string /*i_path*/,
                std::string i_type="file");

/***
Convert chip config file into json data
* i_path: path to file
* i_type: chip type
***/
json readChipPixCfg(std::string /*i_path*/,
                    std::string /*chip_type*/);

/***
Convert data file into json data
* i_path: path to file
* i_name: chip name
***/
json readDataFile(std::string /*i_path*/,
                  std::string /*i_name*/);

/***
Check file at specified path and return true/false if the file is/is not exist.
* i_path: path to file
* i_level: level of output message
    - "warning": output warning message and return false
    - "error": output error message and return false
***/
bool checkFile(std::string /*i_path*/,
               std::string i_level="");

#endif //__READFILE_H
