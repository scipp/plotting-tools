#ifndef __HISTOGAUS_H
#define __HISTOGAUS_H

#include "Histo1D.h"

class HistoGaus : public Histo1D
{
public:
    /// Constructor
    HistoGaus();
    /// Deconstructor
    ~HistoGaus();

private:
    void setParametersX(TH2* /*i_h*/);
    void drawHisto();
    void printHisto(std::string /*i_dir*/,
                    bool        i_print=false,
                    std::string i_ext="png");

    void fillHisto(json& /*i_j*/);
    void fillHisto(TH2*  /*i_h*/);
};
#endif //__HISTOGAUS_H
