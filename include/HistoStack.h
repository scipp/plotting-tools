#ifndef __HISTOSTACK_H
#define __HISTOSTACK_H

#include "Histo1D.h"

class HistoStack : public Histo1D
{
public:
    /// Constructor
    HistoStack();
    /// Deconstructor
    ~HistoStack();

protected:
    void writeHisto();
    virtual void buildHisto();
    void setCanvas();
    virtual void drawHisto();
    virtual void printHisto(std::string /*i_dir*/,
                            bool        i_print=false,
                            std::string i_ext="png");
    void setHisto(TH1* /*i_h*/, std::string /*i_n*/);
    void setHisto(TH2* /*i_h*/, std::string /*i_n*/);

    std::vector<TH1*> m_vec_h;
    THStack* m_hs;
};

#endif //__HISTOSTACK_H
