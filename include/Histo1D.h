#ifndef __HISTO1D_H
#define __HISTO1D_H

#include "PlotTool.h"

class Histo1D : public PlotTool
{
public:
    /// Constructor
    Histo1D();
    /// Deconstructor
    ~Histo1D();

protected:
    virtual void setParameters(json& /*i_j*/);
    virtual void setParameters(TH1*  /*i_h*/);
    virtual void setParameters(TH2*  /*i_h*/);
    virtual void setParametersX(TH2*  /*i_h*/);

    virtual void setHisto(TH1*        /*i_h*/,
                          std::string /*i_n*/);
    virtual void setHisto(TH2*        /*i_h*/,
                          std::string /*i_n*/);

    virtual void writeHisto();
    virtual void buildHisto();
    virtual void setCanvas();
    virtual void drawHisto();
    virtual void printHisto(std::string /*i_dir*/,
                            bool        i_print=false,
                            std::string i_ext="png");
    virtual void fillHisto(json& /*i_j*/);
    virtual void fillHisto(TH1*  /*i_h*/);
    virtual void fillHisto(TH2*  /*i_h*/);
};
#endif //__HISTO1D_H
