#ifndef __HISTOTDAC_H
#define __HISTOTDAC_H

#include "Histo2D.h"
#include "HistoTDACStack.h"

class HistoTDAC : public Histo2D
{
public:
    /// Constructor
    HistoTDAC();
    /// Deconstructor
    ~HistoTDAC();

protected:
    void setParameters(TH2* /*i_h*/);
    void fillHisto(json& /*i_j*/);
    void fillHisto(TH2*  /*i_h*/);
    void drawHisto();
    void printHisto(std::string /*i_dir*/,
                    bool        i_print=false,
                    std::string i_ext="png");
private:
    std::unique_ptr<PlotTool> m_pS;
};
#endif //__HISTOTDAC_H
