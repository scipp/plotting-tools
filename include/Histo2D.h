#ifndef __HISTO2D_H
#define __HISTO2D_H

#include "PlotTool.h"

class Histo2D : public PlotTool
{
public:
    /// Constructor
    Histo2D();
    /// Deconstructor
    ~Histo2D();

protected:
    virtual void setParameters(json& /*i_j*/);
    virtual void setParameters(TH1*  /*i_h*/);
    virtual void setParameters(TH2*  /*i_h*/);

    virtual void writeHisto();
    virtual void buildHisto();
    virtual void setCanvas();
    virtual void drawHisto();

    /// Fill histogram from data
    virtual void fillHisto(json& /*j*/);
    virtual void fillHisto(TH1* /*h*/);
    virtual void fillHisto(TH2* /*h*/);

    virtual void printHisto(std::string /*i_dir*/,
                            bool        i_print=false,
                            std::string i_ext="png");
};

#endif //__HISTO2D_H
