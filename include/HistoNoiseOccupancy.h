#ifndef __HISTONOISEOCCUPANCY_H
#define __HISTONOISEOCCUPANCY_H

#include "Histo1D.h"

class HistoNoiseOccupancy : public Histo1D
{
public:
    HistoNoiseOccupancy();
    ~HistoNoiseOccupancy();

private:
    void setParametersX(TH2* /*i_h*/);
    void drawHisto();

    void fillHisto(json& /*i_j*/);
    void fillHisto(TH2*  /*i_h*/);

    int whichBin(double /*value*/);
};
#endif //__HISTONOISEOCCUPANCY_H
