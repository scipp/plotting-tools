#ifndef __HISTOTDACSTACK_H
#define __HISTOTDACSTACK_H

#include "HistoStack.h"

class HistoTDACStack : public HistoStack
{
public:
    /// Constructor
    HistoTDACStack();
    /// Deconstructor
    ~HistoTDACStack();

private:
    void buildHisto();
    void drawHisto();
    void printHisto(std::string /*i_dir*/,
                    bool        i_print=false,
                    std::string i_ext="png");
};

#endif //__HISTOSTACK_H
