#ifndef __HISTORMS_H
#define __HISTORMS_H

#include "Histo1D.h"

class HistoRms : public Histo1D
{
public:
    /// Constructor
    HistoRms();
    /// Deconstructor
    ~HistoRms();

private:
    void setParametersX(TH2* /*i_h*/);
    void drawHisto();
    void printHisto(std::string /*i_dir*/,
                    bool        i_print=false,
                    std::string i_ext="png");
    void fillHisto(json& /*i_j*/);
    void fillHisto(TH2*  /*i_h*/);
};
#endif //__RMS_H
